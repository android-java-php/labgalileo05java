/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package añobisiesto;

import java.awt.event.KeyEvent;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;

/**
 *
 * @author Josue Daniel Roldan Ochoa.
 */
public class AñiBissto extends javax.swing.JFrame {
private String bisiesto;
    /**
     * Creates new form AñiBissto
     */
    public AñiBissto() {
        this.setLocationRelativeTo(null);
        initComponents();
    }


    
private void añobisiesto(){
if(txnumero.getText().isEmpty()){
JOptionPane.showMessageDialog(null,"Ingrese un Numero");
}else{
String numero= txnumero.getText();
int num= Integer.parseInt(numero);
GregorianCalendar calendar = new GregorianCalendar();
if (calendar.isLeapYear(num)){
bisiesto= "Verdadero";
lbrespuesta.setText(""+bisiesto);
}
else {
bisiesto= "Falso";
lbrespuesta.setText(""+bisiesto);
}
}
}
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txnumero = new javax.swing.JTextField();
        btaceptar = new javax.swing.JButton();
        lbrespuesta = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(102, 102, 0));
        setUndecorated(true);

        txnumero.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txnumero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txnumeroKeyPressed(evt);
            }
        });

        btaceptar.setText("Aceptar");
        btaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btaceptarActionPerformed(evt);
            }
        });

        lbrespuesta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbrespuesta.setText("Verdadero o Falso");

        jMenu1.setText("Salir");

        jMenuItem1.setText("Salir");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txnumero)
                    .addComponent(lbrespuesta, javax.swing.GroupLayout.DEFAULT_SIZE, 308, Short.MAX_VALUE)
                    .addComponent(btaceptar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(txnumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lbrespuesta)
                .addGap(18, 18, 18)
                .addComponent(btaceptar)
                .addContainerGap(32, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btaceptarActionPerformed
  añobisiesto();      // TODO add your handling code here:
    }//GEN-LAST:event_btaceptarActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
    System.exit(0);     // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void txnumeroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txnumeroKeyPressed
      int k = (int) evt.getKeyChar();
      
        if (k > 58 && k < 255) {//Si el caracter ingresado es una letra
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
            JOptionPane.showMessageDialog(null, "Solo puede Ingresar numeros!!!", "Validando Datos",
                JOptionPane.ERROR_MESSAGE);
        

        }   
    
        int d = (int) evt.getKeyChar();
        if (d > 32 && d < 47) {//Si el caracter ingresado es una letra
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
            JOptionPane.showMessageDialog(null, "Solo puede Ingresar numeros!!!", "Validando Datos",
                JOptionPane.ERROR_MESSAGE);
       

        }     // TODO add your handling code here:
    }//GEN-LAST:event_txnumeroKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AñiBissto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AñiBissto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AñiBissto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AñiBissto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AñiBissto().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btaceptar;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JLabel lbrespuesta;
    private javax.swing.JTextField txnumero;
    // End of variables declaration//GEN-END:variables
}
